$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Login de Usuario Melhoria de cenário bonito",
  "description": "",
  "id": "login-de-usuario-melhoria-de-cenário-bonito",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 2,
      "name": "@login"
    }
  ]
});
formatter.before({
  "duration": 29915603208,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Logar no app",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 5,
  "name": "que eu escreva as informações do usuário",
  "keyword": "Dado "
});
formatter.match({
  "location": "LoginSteps.que_eu_escreva_as_informações_do_usuário()"
});
formatter.result({
  "duration": 1930211625,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Login com sucesso",
  "description": "",
  "id": "login-de-usuario-melhoria-de-cenário-bonito;login-com-sucesso",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 7,
      "name": "@loginSucess"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "escrever os dados da senha com \"123456\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 10,
  "name": "valido o texto do botao salvar",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "123456",
      "offset": 32
    }
  ],
  "location": "LoginSteps.escrever_os_dados_da_senha_cpm(String)"
});
formatter.result({
  "duration": 2584295791,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.valido_o_texto_do_botao_salvar()"
});
formatter.result({
  "duration": 1236878875,
  "status": "passed"
});
});