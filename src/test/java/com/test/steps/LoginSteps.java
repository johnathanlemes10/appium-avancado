package com.test.steps;

import com.test.screen.ScreenLogin;
import com.test.screen.Utils;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class LoginSteps {

    ScreenLogin login = new ScreenLogin();
    Utils utils = new Utils();

    @Dado("^que eu escreva as informações do usuário$")
    public void que_eu_escreva_as_informações_do_usuário() {
//        for (int x = 0; x <= 2; x++ ){
            login.writeEmail();
//        }
    }

    @Quando("^escrever os dados da senha com \"([^\"]*)\"$")
    public void escrever_os_dados_da_senha_cpm(String txt) {
//        int i = 1;
//        while (i <=2){
        login.writePass(txt);
        login.clickButtonEnter();
//        i++;
//        }
    }

    @Entao("^valido o texto do botao salvar$")
    public void valido_o_texto_do_botao_salvar()  {
//        utils.scrollToTextIOS(login.elementoios);
          utils.scrollToTextAndroid("55552 - Lucas Dira");
    }

//    @Entao("^valido o texto do botáo \"([^\"]*)\"$")
//    public void valido_o_texto_do_botáo(String txt) {
//        utils.scrollToTextAndroid("55552 - Lucas Dira");
////        utils.validaTxt(login.buttonSave(), txt);
//    }


    @Dado("^que eu preencha o email com o usuário \"([^\"]*)\"$")
    public void que_eu_preencha_o_email_com_o_usuário(String arg1) {
        login.writeEmail2(arg1);
    }
}