package com.test;

import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class Hooks {

    private static AppiumDriver<?> driver;
    public static String platform = "";

    public static AppiumDriver<?> validateDriver() throws MalformedURLException {

        platform = System.getProperty("platform");
        System.out.println("Your platform is:" + platform);

        if(platform.equals("ios")) {

            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("app", "/Users/johnathanf/Documents/appium-com-cucumber/apps/qazandoapp.app");
            capabilities.setCapability("deviceName", "iPhone 14 Pro Max");
            capabilities.setCapability("platformName", "iOS");
            capabilities.setCapability("platformVersion", "16.2");
            capabilities.setCapability("automationName", "XCUITest");
            driver = new IOSDriver(new URL("http://localhost:4723/wd/hub"), capabilities);

        } else if(platform.equals("android")){
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("app", "/Users/johnathanf/Documents/appium-com-cucumber/apps/app-release.apk");
            capabilities.setCapability("deviceName", "emulator-5554");
            capabilities.setCapability("platformName", "Android");
            driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);

        } else if(platform.equals("devicefarm")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            //set your acess credentils
            capabilities.setCapability("browserstack.user", "johnathanlemes_zcT4sa");
            capabilities.setCapability("browserstack.key", "yrozpmvG758J8Uktd3rt");

            //Set URL of the application under test
            capabilities.setCapability("app", "bs://6694edad2bb09a1963644aab6ff3ed62aa34d336");

            //Specify device and os_version for testing
            capabilities.setCapability("device", "Google Pixel 3");
            capabilities.setCapability("os_version", "9.0");
            driver = new AndroidDriver(new URL("http://hub.browserstack.com/wd/hub"), capabilities);
        } else{
            System.out.println("Platform not found, utils android or ios");
        }
            return driver;
    }

    public static AppiumDriver<?> getDriver(){
        return driver;
    }

    public static void quitDriver() {
        if(driver != null) {
            driver.quit();
        }
    }

    @Before
    public static void startProject() throws MalformedURLException {
        if (getDriver() != null){
            getDriver().launchApp();
        } else {
            validateDriver();
        }
    }
}