package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenLogin extends BaseScreen {

    @AndroidFindBy(accessibility = "email")
    @iOSFindBy(xpath = "//XCUIElementTypeTextField[@name=\"email\"]")
    private RemoteWebElement email;

    @AndroidFindBy(accessibility = "senha")
    @iOSFindBy(xpath = "//XCUIElementTypeSecureTextField[@name=\"senha\"]")
    private RemoteWebElement senha;

    @AndroidFindBy(accessibility = "entrar")
    @iOSFindBy(accessibility = "entrar")
    private RemoteWebElement entrar;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"salvar\"")
    @iOSFindBy(accessibility = "(//XCUIElementTypeOther[@name=\"55555\"])[2]")
    private RemoteWebElement salvar;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"salvar\"")
    @iOSFindBy(xpath = "(//XCUIElementTypeOther[@name=\"55552\"])[2]")
    public RemoteWebElement elementoios;

    public void writeEmail() {
        email.sendKeys("teste@teste.com");
    }

    public void writePass(String txt) {
        senha.sendKeys(txt);
    }

    public void clickButtonEnter() {
        entrar.click();
    }

    public void writeEmail2(String txt) {
        email.sendKeys(txt);
    }

    public void buttonSave() {
        salvar.getText();
    }
}
