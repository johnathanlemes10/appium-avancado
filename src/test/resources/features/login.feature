#language: pt
@login
Funcionalidade: Login de Usuario Melhoria de cenário bonito
  Contexto: Logar no app
    Dado que eu escreva as informações do usuário

  @loginSuccess
  Cenário: Login com sucesso
    Quando escrever os dados da senha com "123456"
    Entao valido o texto do botao salvar


  @loginFalied
  Esquema do Cenário: Login com sucesso
    Dado que eu preencha o email com o usuário "<usuario>"
    Quando escrever os dados da senha com "<senha>"

    Exemplos:
    |senha   | usuario              |
    |123456  | qazando@gmail.com    |
    |23423423| John@gmail.com       |
    |1231234 | qazando1234@gmail.com|